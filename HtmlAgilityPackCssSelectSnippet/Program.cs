﻿using System;
using System.Linq;
using HtmlAgilityPack;
using ScrapySharp.Extensions;

namespace HtmlAgilityPackCssSelectSnippet
{
    class Program
    {
        private const string 
            ResourcePath = "../samples/startbootstrap-freelancer-gh-pages-cut.html",
            CssString = "div[id='success'] button[class*='btn-primary']";

        static void Main(string[] args)
        {
            try
            {
                var document = new HtmlDocument();
                document.Load(ResourcePath);

                var elements = document.DocumentNode.CssSelect(CssString);
                if (elements.Count() == 0)
                    throw new Exception("Element not found");

                var element = elements.First();
                Console.WriteLine($"Successfully found. InnerText: {element.InnerText}");
                var attributesData = string.Join(", ", element.Attributes.Select(attribute => $"{attribute.Name} = {attribute.Value}"));
                Console.WriteLine(attributesData);
            }
            catch(Exception ex) 
            {
                Console.WriteLine("Error trying to find element by css selector, Message: {0}", ex.Message);
            }
        }
    }
}
