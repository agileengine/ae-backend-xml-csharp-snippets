﻿using System;
using System.Linq;
using HtmlAgilityPack;

namespace HtmlAgilityPackFindByIdSnippet
{
    class Program
    {
        private const string 
            ResourcePath = "../samples/startbootstrap-freelancer-gh-pages-cut.html",
            TargetElementId = "sendMessageButton";

        static void Main(string[] args)
        {
            try
            {
                var document = new HtmlDocument();
                document.Load(ResourcePath);

                var element = document.GetElementbyId(TargetElementId);
                if (element == null)
                    throw new Exception("Element not found");

                Console.WriteLine($"Successfully found. InnerText: {element.InnerText}");
                var attributesData = string.Join(", ", element.Attributes.Select(attribute => $"{attribute.Name} = {attribute.Value}"));
                Console.WriteLine(attributesData);
            }
            catch(Exception ex) 
            {
                Console.WriteLine("Error trying to find element by id, Message: {0}", ex.Message);
            }
        }
    }
}
