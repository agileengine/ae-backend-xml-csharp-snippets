# AgileEngine backend-XML csharp snippets

It is built on top of [HtmlAgilityPack](http://html-agility-pack.net/) and [ScrapySharp.Extensions](https://www.nuget.org/packages/ScrapySharp/).

You can use HtmlAgilityPack and ScrapySharp for your solution or apply any other convenient library. 